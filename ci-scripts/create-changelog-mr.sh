#!/usr/bin/env bash

# External environment variables
VERSION_CODE=$VERSION_CODE
DEPLOY_KEY_ANDROID_REPO=$deploy_key_android_repo
API_TOKEN=$gitlab_api_access_token

# Make the changelog and create empty CURRENT_VERSION.txt file
# These bash commands react on the following command from .build_job in .gitlab-ci.yml
# mv ./fastlane/metadata/android/en-GB/changelogs/CURRENT_VERSION.txt "./fastlane/metadata/android/en-GB/changelogs/$VERSION_CODE.txt"
touch ./fastlane/metadata/android/en-US/changelogs/CURRENT_VERSION.txt
git add \
  ./fastlane/metadata/android/en-US/changelogs/CURRENT_VERSION.txt \
  "./fastlane/metadata/android/en-US/changelogs/$VERSION_CODE.txt"

RELEASE_NOTES_BRANCH_NAME="docs/release-notes-$VERSION_CODE"

eval "$(ssh-agent -s)"
ssh-add <(echo "$DEPLOY_KEY_ANDROID_REPO")
mkdir -p ~/.ssh
echo -e "Host *\\n\\tStrictHostKeyChecking no\\n\\n" > ~/.ssh/config

git remote set-url origin "https://gitlab.com/fajarsujai/myandroid-cicd.git"
git config --global user.email "fajar.s.fratama@gmail.com"
git config --global user.name "Fajar SUjai"
git checkout -b "$RELEASE_NOTES_BRANCH_NAME"
git add "./fastlane/metadata/android/en-US/changelogs/$VERSION_CODE.txt"
git commit -m "[skip ci] Add changelog for $VERSION_CODE !$CI_MERGE_REQUEST_IID"
git push --set-upstream origin "$RELEASE_NOTES_BRANCH_NAME"

# Extract the host where the server is running, and add the URL to the APIs
[[ $CI_PROJECT_URL =~ ^https?://[^/]+ ]] && HOST="${BASH_REMATCH[0]}/api/v4/projects/"

# Look which is the default branch
TARGET_BRANCH=main

# The description of our new MR, we want to remove the branch after the MR has
# been closed
BODY="{
    \"id\": ${CI_PROJECT_ID},
    \"source_branch\": \"${RELEASE_NOTES_BRANCH_NAME}\",
    \"target_branch\": \"${TARGET_BRANCH}\",
    \"remove_source_branch\": true,
    \"title\": \"Add changelog for ${VERSION_CODE}\",
    \"description\": \"This MR was automatically created from CI, ${CI_JOB_URL}\\n\\nWe moved \`CURRENT_VERSION.txt\` -> \`${VERSION_CODE}.txt\`\",
    \"assignee_id\":\"${GITLAB_USER_ID}\"
}";

# Require a list of all the merge request and take a look if there is already
# one with the same source branch
LISTMR=$(curl --silent "${HOST}${CI_PROJECT_ID}/merge_requests?state=opened" --header "PRIVATE-TOKEN:${API_TOKEN}");
COUNTBRANCHES=$(echo "$LISTMR" | grep -o "\"source_branch\":\"${SOURCE_BRANCH}\"" | wc -l);

# No MR found, let's create a new one
if [ "$COUNTBRANCHES" -eq "0" ]; then
    curl -X POST "${HOST}${CI_PROJECT_ID}/merge_requests" \
        --header "PRIVATE-TOKEN:${API_TOKEN}" \
        --header "Content-Type: application/json" \
        --data "${BODY}";

    echo "Opened a new merge request: WIP: ${SOURCE_BRANCH} and assigned to you";
    exit;
fi

echo "No new merge request opened";
